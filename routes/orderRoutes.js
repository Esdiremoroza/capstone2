const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

router.post('/orderist', (request, response)=>{
	OrderController.createOrder(request,response);
})

router.get('/all', auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllOrders(request, response);
});

module.exports = router;