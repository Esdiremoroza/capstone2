const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

router.post('/registration', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

router.post('/userdetails', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

router.put('/makeadmin/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.makeAdmin(request, response);
})

module.exports = router;