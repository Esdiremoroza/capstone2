const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

router.post('/createproducts', auth.verify, auth.verifyAdmin,(request, response) => {
	ProductController.createProduct(request, response)
});

router.get('/allproducts', (request, response) => {
	ProductController.getAllProducts(request, response);
});

router.get('/activeproducts',(request,response)=>{
	ProductController.getAllActiveProducts(request,response);
})

router.get('/singleproduct/:id',(request,response)=>{
	ProductController.getSingleProduct(request,response);
})

router.put('/updateproduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

router.put('/archiveproduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);
})

router.put('/activateproduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})

module.exports = router;