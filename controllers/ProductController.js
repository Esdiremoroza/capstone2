const Product = require('../models/Product.js');

module.exports.createProduct = (request, response) => {
	let new_product = new Product({
		productName: request.body.productName,
		description: request.body.description,
		price: request.body.price
	});

	return new_product.save().then((saved_product, error) => {
		if(error){
			return response.send(false);
		}

		return response.send({message :"Product added successfully!"});
	}).catch(error => response.send(error));
}

module.exports.getAllProducts = (request,response) =>{
	return Product.find({}).then(result=>{
		return response.send(result);
	})
}

module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.getSingleProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

module.exports.updateProduct = (request, response) => {
	let updated_product_details = {
		productName: request.body.productName,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been updated successfully!'
		})
	})
}

module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: false }).then((course, error) => {
		if(error){
			return response.send(false);
		}

		return response.send({message: "Archived Product"});
	})
}

module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: true }).then((course, error) => {
		if(error){
			return response.send(false);
		}

		return response.send({message: "Product Activated"});
	})
}