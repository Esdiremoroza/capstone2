const Order = require('../models/Order.js');

// module.exports.createOrder = (request_body) => {
// 	let new_order = new Order({
// 		userId: request_body.userId,
// 		products:{
// 			productId: request_body.productId,
// 			quantity: request_body.quantity
// 		},
// 		totalAmount: request_body.totalAmount
// 	});

// 	return new_order.save().then((registered_order, error) => {
// 		if(error){
// 			return {
// 				message: error.message
// 			};
// 		}

// 		return {
// 			message: 'Ordered Successfully'
// 		};
// 	}).catch(error => console.log(error));
// }

module.exports.createOrder = (request, response) => {
	let new_order = new Order({
		userId: request.body.userId,
		products:[{
			productId: request.body.productId,
			quantity: request.body.quantity
		}],
		totalAmount: request.body.totalAmount
	});

	return new_order.save().then((saved_order, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	}).catch(error => response.send(error));
}



module.exports.getAllOrders = (request, response) => {
	return Order.find({}).then(result => {
		return response.send(result);
	})
}