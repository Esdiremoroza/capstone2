const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) =>{
	return User.findOne({email: request.body.email}).then(result=>{
		if (result == null){
			return response.send({
				message:"This Email is not yet registered"
			})
		}
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error=> response.send(error));
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";

		return user;
	}).catch(error => console.log(error));
}

module.exports.makeAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id, { isAdmin: true }).then((course, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}