// Server initializtion Var start
const express = require('express');
const mongoose = require('mongoose');
const cors = require ('cors');
require('dotenv').config();
const port =4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const app = express();
// Server initializtion Var end

// Middleware start 
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cors())
// Middleware end

// Routes Start
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
// Routes End

// DB connection start
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-remoroza.skg2nqh.mongodb.net/capstone-2-api?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log('Connected to the Database!'));
// DB connection end


app.listen(process.env.PORT || port, () => {
	console.log(`Booking System API is now runing at localhost:${process.env.PORT || port}`);
});

module.exports = app;